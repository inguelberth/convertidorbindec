package org.inguelberthgarcia.sistema;

import org.inguelberthgarcia.utilidades.Convertidor;

public class Principal{
	public static void main(String[] args){
		if(args.length>0){
			String valor=args[0];
			if(Convertidor.isBinary(valor))
				System.out.println(Convertidor.convertBinDec(valor));
			else
				System.out.println("El parametro debe ser un binario.");			
		}else
			System.out.println("No se le paso ningun parametro.");
	}
}
