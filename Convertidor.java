package org.inguelberthgarcia.utilidades;

/**
Esta clase sirve como convertidor dentro del paquete utilidades
@author Inguelberth Garcia
*/
public class Convertidor{
	/**
		Metodo para convertir de binario a derimal.
		@param binario Binario a convertir a decimal
		@return Resultado de la convercion (numero decimal)
	*/
	public static int convertBinDec(String binario){
		int resultado=0;
		int numero=1;
		for(int contador=binario.length()-1;contador>=0;contador--){
			resultado=resultado+Integer.parseInt(binario.substring(contador, contador+1))*numero;
			numero=numero*2;
		}
		return resultado;
		
	}
	/**
		Metodo para comprobar si es binario
		@param valor Cadena a comprobar
		@return si es binario o no (true|false)
	*/
	public static boolean isBinary(String valor){
		try{
			for(char numero : valor.toCharArray())
				if(Integer.parseInt(Character.toString(numero)) > 1)
					return false;
			
		}catch(NumberFormatException numex){
			return false;
		}
		return true;
	}
}
